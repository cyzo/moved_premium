<?php
$webRoot = $_SERVER["DOCUMENT_ROOT"]."/_pcRenewal";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>名前｜グラビアギャラリー｜サイゾーpremium</title>
<?php include_once($webRoot."/dev/include/head_share.html"); ?>
<meta name="description" content="視点をリニューアルする情報サイト「サイゾーpremium」" />
<meta name="keyword" content="サイゾー,芸能,タブー" />
<meta property="fb:admins" content="" />
<meta property="fb:app_id" content="" />
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="サイゾーpremium" />
<meta property="og:description" content="視点をリニューアルする情報サイト「サイゾーpremium」" />
<meta property="og:url" content="http://www.premiumcyzo.com/" />
<meta property="og:site_name" content="" />
<meta property="og:image" content="/img/icon-sns.png" />
<meta property="image_src" content="/img/icon-sns.png" />
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@">
<meta name="twitter:url" content="">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:image" content="/img/icon-sns.png">
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.premiumcyzo.com/i/">
</head>
<body id="premiumcyzoCom">
<div id="wrapper" class="gallery">
	<div id="header">
		<h1>h1のテキストを決めてください。</h1>
		<?php include_once($webRoot."/dev/include/header.html"); ?>
	</div><!--header-->

	<div id="contents">
		<div id="archive">
			
			<div id="bread">
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://www.premiumcyzo.com/_pcRenewal/" itemprop="url"><span itemprop="title">サイゾーPremium</span></a></span>
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;&gt;&nbsp;<a href="/_pcRenewal/gallery.php" itemprop="url"><span itemprop="title">グラビアギャラリー</span></a></span>
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;&gt;&nbsp;<span itemprop="title">名前</span></span>
			</div>
			
			<div id="galleryThumbs">
				<a href="#" class="prev split">&nbsp;</a>
				<div class="galleryThumbsBox">
					<ul>
						<li><a href="http://www.premiumcyzo.com/gallery/2012/01/post-56.php?photo=gallery-p1"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1201_g_marusa1-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2012/01/post-56.php?photo=gallery-p2"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1201_g_marusa2-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2012/01/post-56.php?photo=gallery-p3"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1201_g_marusa3-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/12/post-55.php?photo=gallery-p1"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1112_g_p1-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/12/post-54.php?photo=gallery-p1"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1112_g_marusa1-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/12/post-54.php?photo=gallery-p2"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1112_g_marusa2-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/12/post-54.php?photo=gallery-p3"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1112_g_marusa3-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/12/post-53.php?photo=gallery-p1"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1112_g_p2-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/11/post-52.php?photo=gallery-p1"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1111_g_ske1-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/11/post-52.php?photo=gallery-p2"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1111_g_ske2-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/11/post-51.php?photo=gallery-p1"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1111_g_shitagi1-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/11/post-51.php?photo=gallery-p2"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1111_g_shitagi2-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/11/post-48.php?photo=gallery-p1"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1111_g_p1-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/11/post-47.php?photo=gallery-p1"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1111_g_marusa1-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/11/post-47.php?photo=gallery-p2"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1111_g_marusa2-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/11/post-47.php?photo=gallery-p3"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1111_g_marusa3-thumb-84xauto.jpg" alt="" width="84"></a></li>
						<li><a href="http://www.premiumcyzo.com/gallery/2011/11/post-47.php?photo=gallery-p4"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1111_g_marusa4-thumb-84xauto.jpg" alt="" width="84"></a></li>
					</ul>
				</div>
				<a href="#" class="next split">&nbsp;</a>
			</div>
			
			<div id="galleryLarge">
				<div class="galleryPhoto">
					<div id="gallery-p1">
						<div class="photoVisual"><a href="http://www.premiumcyzo.com/modules/member/2011/11/post_2852/"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1201_g_marusa1-thumb-770xauto.jpg" alt="" width="770" height="539" /></a></div>
						<div class="photoCaption"><h2><a href="http://www.premiumcyzo.com/modules/member/2011/11/post_2852/">タレント きゃりーぱみゅぱみゅ</a></h2></div>
						<a class="photoPrev split" href="http://www.premiumcyzo.com/gallery/2011/12/post-53.php"></a> 
						<a class="photoNext split" href="#gallery-p2"></a>
						<a class="photoZoom split" href="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1201_g_marusa1-thumb-1500x1050.jpg"></a>
					</div>
				</div>
				<div class="galleryPhoto">
					<div id="gallery-p2">
						<div class="photoVisual"><a href="http://www.premiumcyzo.com/modules/member/2011/11/post_2852/"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1201_g_marusa2-thumb-770xauto.jpg" alt="" width="770" height="539" /></a></div>
						<div class="photoCaption"><h2><a href="http://www.premiumcyzo.com/modules/member/2011/11/post_2852/">タレント きゃりーぱみゅぱみゅ</a></h2></div>
						<a class="photoPrev split" href="#gallery-p1"></a> 
						<a class="photoNext split" href="#gallery-p3"></a>
						<a class="photoZoom split" href="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1201_g_marusa2-thumb-1500x1050.jpg"></a>
					</div>
				</div>
				<div class="galleryPhoto">
					<div id="gallery-p3">
						<div class="photoVisual"><a href="http://www.premiumcyzo.com/modules/member/2011/11/post_2852/"><img src="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1201_g_marusa3-thumb-770xauto.jpg" alt="" width="770" height="539"/></a></div>
						<div class="photoCaption"><h2><a href="http://www.premiumcyzo.com/modules/member/2011/11/post_2852/">タレント きゃりーぱみゅぱみゅ</a></h2></div>
						<a class="photoPrev split" href="#gallery-p2"></a> 
						<a class="photoNext split" href="http://www.premiumcyzo.com/gallery/2011/11/post-51.php"></a>
						<a class="photoZoom split" href="http://www.premiumcyzo.com/gallery/assets_c/2012/04/1201_g_marusa3-thumb-1500x1050.jpg"></a>
					</div>
				</div>
			</div>
			
			<div id="galleryArchive"></div>
		</div><!--archive-->
	</div><!--contents-->

	<div id="footer">
		<?php include_once($webRoot."/dev/include/footer.html"); ?>
	</div><!--footer-->
</div><!--wrapper-->
<?php include_once($webRoot."/dev/include/gNav.html"); ?>
<?php include_once($webRoot."/dev/include/foot_script.html"); ?>
<script type="text/javascript" src="/_pcRenewal/js/gallery.min.js" ></script>
</body>
</html>