<?php
$webRoot = $_SERVER["DOCUMENT_ROOT"]."/_pcRenewal";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>(●ページ目)カテゴリー名｜サイゾーpremium</title>
<?php include_once($webRoot."/dev/include/head_share.html"); ?>
<meta name="description" content="視点をリニューアルする情報サイト「サイゾーpremium」" />
<meta name="keyword" content="サイゾー,芸能,タブー" />
<meta property="fb:admins" content="" />
<meta property="fb:app_id" content="" />
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="サイゾーpremium" />
<meta property="og:description" content="視点をリニューアルする情報サイト「サイゾーpremium」" />
<meta property="og:url" content="http://www.premiumcyzo.com/" />
<meta property="og:site_name" content="" />
<meta property="og:image" content="/img/icon-sns.png" />
<meta property="image_src" content="/img/icon-sns.png" />
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@">
<meta name="twitter:url" content="">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:image" content="/img/icon-sns.png">
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.premiumcyzo.com/i/">
</head>
<body id="premiumcyzoCom">
<div id="wrapper" class="category">
	<div id="header">
		<h1>ここがh1テキストです</h1>
		<?php include_once($webRoot."/dev/include/header.html"); ?>
	</div><!--header-->
	
	<div id="contents">
		
		<div id="archive">
			
			<div id="bread">
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://www.premiumcyzo.com/_pcRenewal/" itemprop="url"><span itemprop="title">サイゾーPremium</span></a></span>
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;&gt;&nbsp;<span itemprop="title">●●●●●●</span></span>
			</div>
			
			<div id="entryTL">

				<div class="entry">
					<div class="txt">
						<div class="catIcon iconIttoku">第1特集</div>
						<div class="subTitle">高視聴率作家・池井戸潤の正体【5】</div>
						<h2><a href="#">『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像</a></h2>
						<div class="entryTags">関連キーワード：<span><a href="#">201407</a></span><span><a href="#">ドラマ</a></span><span><a href="#">ナショナリズム</a></span><span><a href="#">会社</a></span><span><a href="#">出版</a></span><span><a href="#">半沢直樹</a></span><span><a href="#">右翼</a></span><span><a href="#">愛国</a></span><span><a href="#">本</a></span><span><a href="#">池井戸潤</a></span><span><a href="#">鈴木邦男</a></span></div>
						<p>――右傾化エンタメが盛り上がりを見せる昨今、池井戸潤作品もこの流れにくみするという声がある。そこで、ナショナリズムに詳しい政治活動家・鈴木邦男氏に読み解いてもらうと同時に、昨今の日本の右傾化についても...</p>
						<div class="subInfo">
							<div class="readmore"><a href="#">続きを読む</a></div>
							<div class="entryDate">2014.06.29</div>
						</div>
					</div>
					<div class="photo">
						<a href="#"><img alt="『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像" src="http://dummyimage.com/126x160/0065b8/fff.png&text=photo5" height="160"></a>
					</div>
					<div class="newFree new"><span>新着</span></div>
				</div>
				<div class="entry">
					<div class="txt">
						<div class="catIcon iconNitoku">第2特集</div>
						<div class="subTitle">高視聴率作家・池井戸潤の正体【5】</div>
						<h2><a href="#">『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像</a></h2>
						<div class="entryTags">関連キーワード：<span><a href="#">201407</a></span><span><a href="#">ドラマ</a></span><span><a href="#">ナショナリズム</a></span><span><a href="#">会社</a></span><span><a href="#">出版</a></span><span><a href="#">半沢直樹</a></span><span><a href="#">右翼</a></span><span><a href="#">愛国</a></span><span><a href="#">本</a></span><span><a href="#">池井戸潤</a></span><span><a href="#">鈴木邦男</a></span></div>
						<p>――右傾化エンタメが盛り上がりを見せる昨今、池井戸潤作品もこの流れにくみするという声がある。そこで、ナショナリズムに詳しい政治活動家・鈴木邦男氏に読み解いてもらうと同時に、昨今の日本の右傾化についても...</p>
						<div class="subInfo">
							<div class="readmore"><a href="#">続きを読む</a></div>
							<div class="entryDate">2014.06.29</div>
						</div>
					</div>
					<div class="photo">
						<a href="#"><img alt="『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像" src="http://dummyimage.com/126x160/0065b8/fff.png&text=photo5" height="160"></a>
					</div>
					<div class="newFree free"><span>無料</span></div>
				</div>
				<div class="entry">
					<div class="txt">
						<div class="catIcon iconRensai">連載</div>
						<div class="subTitle">高視聴率作家・池井戸潤の正体【5】</div>
						<h2><a href="#">『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像</a></h2>
						<div class="entryTags">関連キーワード：<span><a href="#">201407</a></span><span><a href="#">ドラマ</a></span><span><a href="#">ナショナリズム</a></span><span><a href="#">会社</a></span><span><a href="#">出版</a></span><span><a href="#">半沢直樹</a></span><span><a href="#">右翼</a></span><span><a href="#">愛国</a></span><span><a href="#">本</a></span><span><a href="#">池井戸潤</a></span><span><a href="#">鈴木邦男</a></span></div>
						<p>――右傾化エンタメが盛り上がりを見せる昨今、池井戸潤作品もこの流れにくみするという声がある。そこで、ナショナリズムに詳しい政治活動家・鈴木邦男氏に読み解いてもらうと同時に、昨今の日本の右傾化についても...</p>
						<div class="subInfo">
							<div class="readmore"><a href="#">続きを読む</a></div>
							<div class="entryDate">2014.06.29</div>
						</div>
					</div>
					<div class="photo">
						<a href="#"><img alt="『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像" src="http://dummyimage.com/126x160/0065b8/fff.png&text=photo5" height="160"></a>
					</div>
				</div>
				<div class="entry">
					<div class="txt">
						<div class="catIcon iconNews">ニュース</div>
						<div class="subTitle">高視聴率作家・池井戸潤の正体【5】</div>
						<h2><a href="#">『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像</a></h2>
						<div class="entryTags">関連キーワード：<span><a href="#">201407</a></span><span><a href="#">ドラマ</a></span><span><a href="#">ナショナリズム</a></span><span><a href="#">会社</a></span><span><a href="#">出版</a></span><span><a href="#">半沢直樹</a></span><span><a href="#">右翼</a></span><span><a href="#">愛国</a></span><span><a href="#">本</a></span><span><a href="#">池井戸潤</a></span><span><a href="#">鈴木邦男</a></span></div>
						<p>――右傾化エンタメが盛り上がりを見せる昨今、池井戸潤作品もこの流れにくみするという声がある。そこで、ナショナリズムに詳しい政治活動家・鈴木邦男氏に読み解いてもらうと同時に、昨今の日本の右傾化についても...</p>
						<div class="subInfo">
							<div class="readmore"><a href="#">続きを読む</a></div>
							<div class="entryDate">2014.06.29</div>
						</div>
					</div>
					<div class="photo">
						<a href="#"><img alt="『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像" src="http://dummyimage.com/126x160/0065b8/fff.png&text=photo5" height="160"></a>
					</div>
				</div>
				<div class="entry">
					<div class="txt">
						<div class="catIcon iconInterview">インタビュー</div>
						<div class="subTitle">高視聴率作家・池井戸潤の正体【5】</div>
						<h2><a href="#">『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像</a></h2>
						<div class="entryTags">関連キーワード：<span><a href="#">201407</a></span><span><a href="#">ドラマ</a></span><span><a href="#">ナショナリズム</a></span><span><a href="#">会社</a></span><span><a href="#">出版</a></span><span><a href="#">半沢直樹</a></span><span><a href="#">右翼</a></span><span><a href="#">愛国</a></span><span><a href="#">本</a></span><span><a href="#">池井戸潤</a></span><span><a href="#">鈴木邦男</a></span></div>
						<p>――右傾化エンタメが盛り上がりを見せる昨今、池井戸潤作品もこの流れにくみするという声がある。そこで、ナショナリズムに詳しい政治活動家・鈴木邦男氏に読み解いてもらうと同時に、昨今の日本の右傾化についても...</p>
						<div class="subInfo">
							<div class="readmore"><a href="#">続きを読む</a></div>
							<div class="entryDate">2014.06.29</div>
						</div>
					</div>
					<div class="photo">
						<a href="#"><img alt="『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像" src="http://dummyimage.com/126x160/0065b8/fff.png&text=photo5" height="160"></a>
					</div>
				</div>
				
			</div><!--entryTL-->
			
			<div id="pageNav">
				<div>pages</div>
				<ol>
					<li><a href="#">1</a></li>
					<li><span>2</span></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">6</a></li>
					<li><a href="#">7</a></li>
					<li><a href="#">8</a></li>
					<li><a href="#">9</a></li>
					<li><a href="#">10</a></li>
				</ol>
				<span><a href="#">→</a></span>
			</div>
			
		</div><!--archive-->
		
		<div id="aside">
			<?php include_once($webRoot."/dev/include/aside_archive.html"); ?>
		</div><!--aside-->
	</div><!--contents-->

	<div id="footer">
		<?php include_once($webRoot."/dev/include/footer.html"); ?>
	</div><!--footer-->
</div><!--wrapper-->
<?php include_once($webRoot."/dev/include/gNav.html"); ?>
<?php include_once($webRoot."/dev/include/foot_script.html"); ?>
<script type="text/javascript" src="/_pcRenewal/js/top.min.js" ></script>
</body>
</html>