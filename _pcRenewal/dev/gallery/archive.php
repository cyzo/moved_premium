<?php
if (isset($_GET["page"])) {
	$entNum["c"] = $_GET["page"];
	$num = $_GET["page"] == 1 ? "" : "_".$entNum["c"];
	$incFile = "index".$num.".xml";
}
else {
	$entNum["c"] = 1;
	$incFile = "index.xml";
}
$dir = scandir($_SERVER["DOCUMENT_ROOT"]."/gallery/list/");
$entNum["p"] = count($dir)-2;



$html = "";
$a = 0;
$xmlData = simplexml_load_file($_SERVER["DOCUMENT_ROOT"]."/gallery/list/$incFile");
foreach ($xmlData->channel->item as $data) {
	$datas[$a]["title"] = xmlData2Txt($data->title);
	$datas[$a]["url"]   = xmlData2Txt($data->url);
	$datas[$a]["img"]   = xmlData2Txt($data->img);
	$datas[$a]["date"]  = xmlData2Txt($data->date);
	$a++;
}

foreach ($datas as $key => $value){
	$key_id[$key] = $value['date'];
}
array_multisort($key_id, SORT_DESC, $datas);

$num = 0;
foreach ($datas as $data) {
	$num++;
	$title = $data["title"];
	$img   = $data["img"];
	$url   = $data["url"];
	$date   = $data["date"];
$html .= <<< EOF
	<div class="galleryArchiveBox"><a href="$url?page=${entNum["c"]}">
		<div class="photo"><img src="$img" width="145"></div>
		<div class="txt">$title</div>
	</a></div>

EOF;
	if($num % 6 == 0) {
		$html = $html . '</div><div class="galleryArchiveInner">';
	}
}

print '<div class="gaSubTitle">グラビアギャラリー</div><div class="galleryArchiveInner">'.$html.'</div>'.pageNav($entNum);

function xmlData2Txt($tmp){
return $unko = <<< EOF
$tmp
EOF;
}

function pageNav($entNum){
	$nowPage = $entNum["c"];
	$allPage = $entNum["p"];
	$prev = $next = "";

	if($entNum["c"]==2) {
		$prev = '<li class="prev"><a href="?page='.$entNum["l"].'">前のページ</a></li>';
	}
	elseif($entNum["c"]>2) {
		$prev = '<li class="prev"><a href="?page='.($entNum["c"]-1).'">前のページ</a></li>';
	}
	
	if($entNum["c"]!=$entNum["p"]) {
		$next = '<span><a href="?page='.($entNum["c"]+1).'" data-num="'.($entNum["c"]+1).'">→</a></span>';
	}
	
	if ($nowPage == 1) {
		$sLoop=1;
		$eLoop=$allPage>5?5:$allPage;
	}
	elseif ($nowPage == $allPage) {
		$sLoop=$allPage>5?$nowPage-4:1;
		$eLoop=$allPage;
	}
	elseif ($nowPage-2>=1 && $nowPage+2<=$allPage) {
		$sLoop=$nowPage-2;
		$eLoop=$nowPage+2;
	}
	elseif ($nowPage-3>=1 && $nowPage+1<=$allPage) {
		$sLoop=$nowPage-3;
		$eLoop=$nowPage+1;
	}
	elseif ($nowPage-1>=1 && $nowPage+3<=$allPage) {
		$sLoop=$nowPage-1;
		$eLoop=$nowPage+3;
	}
	else {
		$sLoop=$nowPage-1;
		$eLoop=$allPage;
	}
	for ($a=$sLoop; $a<=$eLoop; $a++) {
		if ($a!=$nowPage) {
			if($a==1){
				$navMiddle .= '<li class="pageLink"><a href="/_pcRenewal/gallery.php" data-num="'.$a.'">'.$a.'</a></li>';
			}
			else {
				$navMiddle .= '<li class="pageLink"><a href="?page='.$a.'" data-num="'.$a.'">'.$a.'</a></li>';
			}
		}
		else {
			$navMiddle .= '<li class="current"><span>'.$a.'</span></li>';
		}
	}

$html = <<< EOF
<div id="pageNav">
	<div>pages</div>
	<ol>
		$navMiddle
	</ol>
	$next
</div>
EOF;

	return $html;
}