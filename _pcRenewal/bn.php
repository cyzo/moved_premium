<?php
$webRoot = $_SERVER["DOCUMENT_ROOT"]."/_pcRenewal";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>(●ページ目)バックナンバー一覧｜サイゾーpremium</title>
<?php include_once($webRoot."/dev/include/head_share.html"); ?>
<meta name="description" content="視点をリニューアルする情報サイト「サイゾーpremium」" />
<meta name="keyword" content="サイゾー,芸能,タブー" />
<meta property="fb:admins" content="" />
<meta property="fb:app_id" content="" />
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="サイゾーpremium" />
<meta property="og:description" content="視点をリニューアルする情報サイト「サイゾーpremium」" />
<meta property="og:url" content="http://www.premiumcyzo.com/" />
<meta property="og:site_name" content="" />
<meta property="og:image" content="/img/icon-sns.png" />
<meta property="image_src" content="/img/icon-sns.png" />
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@">
<meta name="twitter:url" content="">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:image" content="/img/icon-sns.png">
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.premiumcyzo.com/i/">
</head>
<body id="premiumcyzoCom">
<div id="wrapper" class="magazine">
	<div id="header">
		<h1>h1のテキストを決めてください</h1>
		<?php include_once($webRoot."/dev/include/header.html"); ?>
	</div><!--header-->
	
	<div id="contents">
		
		<div id="archive">
			
			<div id="bread">
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://www.premiumcyzo.com/_pcRenewal/" itemprop="url"><span itemprop="title">サイゾーPremium</span></a></span>
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;&gt;&nbsp;<span itemprop="title">バックナンバー一覧</span></span>
			</div>

			<div id="backnumberList">
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">1月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">2月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">3月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：高さ調整チェック</p>
					<p class="tokusyu">第2特集：高さ調整チェック</p>
					<p class="tokusyu">第3特集：高さ調整チェック</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">4月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">5月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">6月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">1月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">2月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">3月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">4月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">5月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				<div class="box">
					<div class="photo"><img src="http://dummyimage.com/167x226/0065b8/fff.png&text=back" alt="title" width="167" height="226"></div>
					<p class="month">6月号　表紙：松浦 雅</p>
					<p class="tokusyu">第1特集：タブーと陰謀論</p>
					<p class="tokusyu">第2特集：タブーと陰謀論</p>
					<div class="link"><a href="#"></a></div>
				</div>
				
				<div id="backnumberListAd"><img src="http://dummyimage.com/468x60/0065b8/fff.png&text=ad" width="468" height="60" alt="title"></div>
			</div><!--backnumberList-->
				
			<div id="pageNav">
				<div>pages</div>
				<ol>
					<li><a href="#">1</a></li>
					<li><span>2</span></li>
					<li><a href="#">3</a></li>
				</ol>
				<span><a href="#">→</a></span>
			</div>
		</div><!--archive-->
		
		<div id="aside">
			<?php include_once($webRoot."/dev/include/aside_archive.html"); ?>
		</div><!--aside-->
	</div><!--contents-->

	<div id="footer">
		<?php include_once($webRoot."/dev/include/footer.html"); ?>
	</div><!--footer-->
</div><!--wrapper-->
<?php include_once($webRoot."/dev/include/gNav.html"); ?>
<?php include_once($webRoot."/dev/include/foot_script.html"); ?>
<script type="text/javascript" src="/_pcRenewal/js/top.min.js" ></script>
</body>
</html>