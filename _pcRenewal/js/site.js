$(function(){
//index loading
	if(!$.cookie("displayEntry")){
		$("#entryLoad").hide();
		$("#subNav li.latest").addClass("current");
		$("#entryTL .latest").fadeIn(500);
	}
	else {
		var entryShow = $.cookie("displayEntry");
		$("#entryLoad").hide();
		$("#subNav li."+entryShow).addClass("current");
		$("#entryTL ."+entryShow).fadeIn(500);
	}
	$("#subNav li a").click(function(){
		var entryHide = $("#subNav li.current").attr("subNav-data");
		$("#subNav li").removeClass("current");
		$("#entryTL ."+entryHide).hide();
		
		var entryShow = $(this).parent("li").attr("subNav-data");
		$("#subNav li."+entryShow).addClass("current");
		$("#entryTL ."+entryShow).fadeIn(500);
		$.cookie("displayEntry", entryShow);
		return false;
	});
//preImgWidth
	$("#entryBody pre[class!=center] .mt-image-none").each(function(){
		var hoge = $(this).width();
		var fuga = $(this).next();
		var fuga2 = $(this).parents("pre").find("pre.stxt");
		if (fuga.attr("class")=="stxt" || fuga.attr("class")=="stxt2") fuga.css("width",hoge+"px");
		if (fuga2.attr("class")=="stxt" || fuga2.attr("class")=="stxt2") fuga2.css("width",hoge+"px");
	});
	$(".entryImgLeft,.entryImgRight,.entryImgCenter").each(function(){
		$(this).css("width",$(this).find("img").width()+"px");
		$(this).find(".stxt").css("width",$(this).find("img").width()+"px");
	});
//img link
	function onOffImg(){
		linkIMG = "#navInner > ul > li > a, #contents a img, #headTools a, #aboutTocana #sns a, #cyzoLink li a";
		$(linkIMG).on({
			'mouseenter':function(){$(this).stop().animate({"opacity":"0.7"},300);},
			'mouseleave':function(){$(this).stop().animate({"opacity":"1"},200);}
		});
	}
	onOffImg();
//pagetop
	$(window).scroll(function() {
		$(this).scrollTop() > 300 ? $("#goPageTop").fadeIn() : $("#goPageTop").fadeOut();
	});
	$("#goPageTop").click(function(){
		$('html, body').animate({scrollTop:0}, 500);
		return false;
	});
//page scroll
	$(".jumpLink").click(function () {
        var p = $($(this).attr("href")).offset().top;
        $('html,body').animate({ scrollTop: p }, 'fast');
        return false;
    });

//ad control
	$("#backnumberList .box:nth-child(6)").after($("#backnumberList #backnumberListAd"));
//backnumber height control
	var bnH = 0;
	var bnN = 1;
	$("#backnumberList .box").each(function(){
		bnH = $(this).height()>bnH ? $(this).height():bnH;
		if (bnN % 2 == 0) {
			bnN2 = bnN - 2;
			$(this).css("height",bnH+"px");
			$(this).find(".link, a").css("height",(bnH+60)+"px");
			$("#backnumberList .box").eq(bnN2).css("height",bnH+"px");
			$("#backnumberList .box").eq(bnN2).find(".link, a").css("height",(bnH+60)+"px");
			bnH = 0;
		}
		bnN++;
	});
//other domain
	$('a[href^="http"]').not('[href^="http://'+location.hostname+'"]').not('[target^="_blank"]').click(function(){
		window.open(this.href, '');
		return false;
	});
	
//pc<->sm
	if($.cookie("viewmode")=="pc"){
		$("#footer").append('<div id="viewChanege"><p>表示切替：<span id="resetViewCookie">スマートフォン版</span> | パソコン版</p></div>');
		$("#resetViewCookie").click(function(){
			$.cookie("viewmode" ,null, { path: "/" });
			location.href = location.href;
			return false;
		});
		function is_mobile () {
		  var useragents = ['Android','BlackBerry','Windows Phone','iPhone'];
		  var pattern = new RegExp(useragents.join('|'), 'i');
		  return pattern.test(navigator.userAgent);
		}
		if (is_mobile()) {
		  var link = document.createElement('link');
		  link.rel = 'stylesheet';
		  link.type = 'text/css';
		  link.href = '/css/sp.css';
		  document.getElementsByTagName('head')[0].appendChild(link);
		}
	}
});
  
//fb
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1&appId=166487183547697";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));










