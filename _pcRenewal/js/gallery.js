$(function(){
	//display control archive
	nowPage = location.search.split("=");
	if(!$.cookie("displayGallery")){
		getGalleryArchive(1);
	}
	else {
		getGalleryArchive($.cookie("displayGallery"));
	}
	
	$(document).on("click", "#pageNav a", function(){
		var page = $(this).attr("data-num");
		getGalleryArchive(page);
        var zzz = setInterval(function() {
			clearInterval(zzz);
	        var p = $("#galleryArchive").offset().top - 20;
	        $('html,body').animate({ scrollTop: p }, 'slow');
		}, 200);
		return false;
	});
	
	function getGalleryArchive(page){
		$.get(
			'/_pcRenewal/dev/gallery/archive.php',
			{
				page : page
			},
			function(data) {
				$('#galleryArchive').html(data);
				imgHover();
				$.cookie("displayGallery", page);
			}
		);
	}
	
	function imgHover(){
		$('#galleryArchive a img, #galleryPhoto a img').on({
			'mouseenter':function(){$(this).stop().animate({"opacity":"0.7"},300);},
			'mouseleave':function(){$(this).stop().animate({"opacity":"1"},200);}
		});
	}
	
	//gallery entry bigIMG display
	function bigIMG(target) {
		var targetImg = target.attr("href");
		var w = $("#wrapper").width() > $("body,html").width() ? $("#wrapper").width()+"px" : $("body,html").width()+"px";
		var h = $("body,html").height()+"px";
		$("body").append('<div id="dark" style="width:'+w+';height:'+h+';"><div id="darkInner"><img src="'+targetImg+'"></div></div>');
		$("#dark").fadeIn(500);
		$(document).on("click","#dark, #darkInner", function(e){
			$("#dark, #darkInner").fadeOut(
				500,
				function(){
					$(this).remove();
				}
			);
			e.stopImmediatePropagation();
		});
	}
	$(".photoZoom").click(function(){
		bigIMG($(this));
		return false;
	});
	$(window).resize(function() {
		if($("#dark")) {
			var w = $("#wrapper").width() > $("body,html").width() ? $("#wrapper").width()+"px" : $("body,html").width()+"px";
			var h = $("body,html").height()+"px";
			$("#dark").css({"width":w, "height":h});
		}
	});
	
	
	//gallery entry bigIMG control
	var showImgNum = location.search!="" ? location.search.replace(/\?p=gallery-p/, "") - 1 : 0;
	var targetWrap = $("#galleryLarge");
	var targetObj  = $("#galleryLarge .galleryPhoto");
	
	targetObj.each(function(){
		$(this).css("height", $(this).find("img").height()+"px");
	});
	targetObj.eq(showImgNum).show();
	targetWrap.css("height", (targetObj.eq(showImgNum).find("img").height()+50)+"px");
	
	$(".photoNext, .photoPrev").click(function(){
		if ($(this).attr("href").match(/#gallery-p.*/)) {
			$(this).parents(".galleryPhoto").fadeOut(500);
			$($(this).attr("href")).parents(".galleryPhoto").fadeIn(500);
			return false;
		}
	});
	
	
	//gallery entry thumb control
	$(".galleryThumbsBox ul").css("width", ($(".galleryThumbsBox li").size()*89)+"px");
	
	if($(".galleryThumbsBox li").size()>10){
		var a_moveTime = 300;
		var a_lTarget = $("#galleryThumbs .prev");
		var a_rTarget = $("#galleryThumbs .next");
		var a_target  = $("#galleryThumbs ul");
		
		a_rTarget.click(function(){
			a_target.stop().animate(
				{"left":"-89px"},
				{
					"duration":a_moveTime,
					"easing":"easeOutCubic",
					"complete":function(){
						a_target.css({"left":"0"}).append(a_target.find("li:first").clone()).find("li:first").remove();
						imgHover();
					}
				}
			);
			return false;
		});
		a_lTarget.click(function(){
			a_target.css({"left":"-89px"}).prepend(a_target.find("li:last").clone()).find("li:last").remove();
			a_target.stop().animate(
				{"left":"0"},
				{
					"duration":a_moveTime,
					"easing":"easeOutCubic",
					"complete":function(){
						imgHover();
					}
				}
			);
			return false;
		});
		
	}
	
	//gallery top mainVisual control
	var moveTime = 500;
	var rTarget = $("#moveRight");
	var lTarget = $("#moveLeft");
	var target1 = $("#galleryPhotoInner");
	var target2 = $("#galleryTextInner");
	var w1      = target1.find("a").size()*1000;
	var w2      = w1+1000;
	
	if(target1.find("a").size()>1){
		target1.css("width",w1+"px");
		target2.css("width",w1+"px");
		
		rTarget.click(function(){
			target1.stop().animate(
				{"left":"-1000px"},
				{
					"duration":moveTime,
					"easing":"easeInQuart",
					"complete":function(){
						target1.css({"left":"0"}).append(target1.find("a:first").clone()).find("a:first").remove();
						imgHover();
					}
				}
			);
			target2.stop().animate(
				{"left":"-1000px"},
				{
					"duration":moveTime,
					"easing":"easeInQuart",
					"complete":function(){
						target2.css({"left":"0"}).append(target2.find("a:first").clone()).find("a:first").remove();
						imgHover();
					}
				}
			);
		});
		lTarget.click(function(){
			target1.css({"left":"-1000px"}).prepend(target1.find("a:last").clone()).find("a:last").remove();
			target2.css({"left":"-1000px"}).prepend(target2.find("a:last").clone()).find("a:last").remove();
			target1.stop().animate(
				{"left":"0"},
				{
					"duration":moveTime,
					"easing":"easeInQuart",
					"complete":function(){
						imgHover();
					}
				}
			);
			target2.stop().animate(
				{"left":"0"},
				{
					"duration":moveTime,
					"easing":"easeInQuart",
					"complete":function(){
						imgHover();
					}
				}
			);
		});
	}
});
