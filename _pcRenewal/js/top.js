$(function(){$(window).bind("load", function(){ 
	if($("#mainvisual #photo img").size()>1){
		var timer = 10000;
		var moveTime = 300;
		var moveDistance = 1;
		
		$("#mainvisual #photoInner").css("width", ($("#mainvisual #photo img").size()*1000)+"px");
		$("#mainvisual #textInner").css("width", ($("#mainvisual #text p").size()*1000)+"px");
		
		function visualMove(moveDistance){
			$("#mainvisual #photoInner").not(":animated").animate(
				{"left": "-1000px"},
				moveTime,
				"swing",
				function(){
					$(this).append($(this).find("a:first").clone());
					$(this).css("left","0").find("a:first").remove();
					isTimer = setTimeout(function(){ visualMove(moveDistance); }, timer);
				}
			);
			
			$("#mainvisual #textInner").not(":animated").animate(
				{"left": "-1000px"},
				moveTime,
				"swing",
				function(){
					$(this).append($(this).find("p:first").clone());
					$(this).css("left","0").find("p:first").remove();
					isTimer = setTimeout(function(){ visualMove(moveDistance); }, timer);
				}
			);
		}
		
		isTimer = setTimeout(function(){ visualMove(1); }, timer);
	}
});});