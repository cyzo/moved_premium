<?php
$webRoot = $_SERVER["DOCUMENT_ROOT"]."/_pcRenewal";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>(●ページ目)記事タイトル｜サイゾーpremium</title>
<?php include_once($webRoot."/dev/include/head_share.html"); ?>
<meta name="description" content="視点をリニューアルする情報サイト「サイゾーpremium」" />
<meta name="keyword" content="サイゾー,芸能,タブー" />
<meta property="fb:admins" content="" />
<meta property="fb:app_id" content="" />
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="サイゾーpremium" />
<meta property="og:description" content="視点をリニューアルする情報サイト「サイゾーpremium」" />
<meta property="og:url" content="http://www.premiumcyzo.com/" />
<meta property="og:site_name" content="" />
<meta property="og:image" content="/img/icon-sns.png" />
<meta property="image_src" content="/img/icon-sns.png" />
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@">
<meta name="twitter:url" content="">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:image" content="/img/icon-sns.png">
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.premiumcyzo.com/i/">
</head>
<body id="premiumcyzoCom">
<div id="wrapper" class="entry beforeLogin">
	<div id="header">
		<div id="copyText">個別記事の場合、divタグになります。</div>
		<?php include_once($webRoot."/dev/include/header.html"); ?>
	</div><!--header-->
	
	<div id="contents">
		
		<div id="archive">
			
			<div id="bread">
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://www.premiumcyzo.com/_pcRenewal/" itemprop="url"><span itemprop="title">サイゾーPremium</span></a></span>
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;&gt;&nbsp;<a href="http://www.premiumcyzo.com/_pcRenewal/" itemprop="url"><span itemprop="title">カテゴリ名</span></a></span>
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;&gt;&nbsp;<span itemprop="title">記事タイトル記事タイトル記事タイトル記事タイトル記事タイトル記事タイトル記事タイトル記事タイトル記事タイトル</span></span>
			</div>
			
			<div id="entryMain">
				
				<div id="entryheader">
					<div class="catIcon iconIttoku">第1特集</div>
					<div class="subTitle">高視聴率作家・池井戸潤の正体【5】</div>
					<h1><a href="#">H1タグ　『半沢直樹』は、ナショナリズムとリンクする？ 【新右翼・鈴木邦男】が語る右傾エンタメの実像</a></h1>
					<div class="entryTags">関連キーワード：<span><a href="#">201407</a></span><span><a href="#">ドラマ</a></span><span><a href="#">ナショナリズム</a></span><span><a href="#">会社</a></span><span><a href="#">出版</a></span><span><a href="#">半沢直樹</a></span><span><a href="#">右翼</a></span><span><a href="#">愛国</a></span><span><a href="#">本</a></span><span><a href="#">池井戸潤</a></span><span><a href="#">鈴木邦男</a></span></div>
					<div class="subInfo">
						<div class="readitlater"><span>+</span><a title="会員登録・ログインするとご利用できます">あとで読む</a></div>
						<div class="entryDate">2014.06.29</div>
					</div>
				</div>
				
				<div id="entryHeaderAd"><img src="http://dummyimage.com/468x60/0065b8/fff.png&text=ad" width="468" height="60" alt="title"></div>
				
				<div id="entryBody">
					<p>――今年4月、「小悪魔ageha」の版元であるインフォレスト社が倒産した。独特すぎる誌面と名物編集長の存在で00年代後半に話題を呼びまくった「ageha」がどうなってしまうのか、と思っていた矢先に今度は「egg」が休刊を発表。いよいよギャル誌は終わりつつあるようだ。あるいは、きゃりーぱみゅぱみゅを生んだ“原宿系”青文字雑誌の隆盛も、ひところに比べれば落ち着き、一方で「CLASSY.」が実売90%を達成するなど再び赤文字的なコンサバへの揺り戻しも見られる。刻々と変化し続ける女性ファッション誌の潮流の中で、現在の女性の消費とライフスタイルに、雑誌はどうコミットできるのか？ 雑誌不況の現在だからこそ考えてみたい。</p>
					
					<pre class="center"><img alt="1407_gal_01.jpg" src="http://www.premiumcyzo.com/update_files/1407_gal_01.jpg" width="520" height="340" class="mt-image-none" style=""><pre class="stxt">（写真／永峰拓也）</pre></pre>
					
					<p><strong>A</strong>　モデル本人も裏で「あんなの効かないよ！」って言ってるのに、ブログでは「これで3キロ痩せました」みたいに、いけしゃあしゃあと宣伝してるやつね（苦笑）。ああいうのも、10代の何も知らない読者は騙されちゃうから。聞いた話だと、アフィリエイトを始めた月の収入が80万円、最高で月250万円っていう子もいたよ。雑誌のギャラって高くても1万円くらいだし、場合によってはギャラなしのところもあるじゃない。それを考えると、そりゃ雑誌に出るのが馬鹿らしくなっちゃうよね。</p>
					<pre class="memo"><pre class="memo-cont"><img class="memo-icon" src="http://www.premiumcyzo.com/common/imgs/memo_icon.gif" alt="MEMO" width="43" height="42"><strong>『W杯』</strong><br>1990年代以降、サッカー人気の高まりとともに注目度も高くなってきたW杯。数々の選手が海外リーグで活躍する中で、予選突破や優勝まで期待されるほどになったが……。</pre><pre class="memo-bottom"><hr></pre></pre>
					<p><strong>A</strong>　モデル本人も裏で「あんなの効かないよ！」って言ってるのに、ブログでは「これで3キロ痩せました」みたいに、いけしゃあしゃあと宣伝してるやつね（苦笑）。ああいうのも、10代の何も知らない読者は騙されちゃうから。聞いた話だと、アフィリエイトを始めた月の収入が80万円、最高で月250万円っていう子もいたよ。雑誌のギャラって高くても1万円くらいだし、場合によってはギャラなしのところもあるじゃない。それを考えると、そりゃ雑誌に出るのが馬鹿らしくなっちゃうよね。</p>
					
					<pre class="right"><a href="http://www.amazon.co.jp/exec/obidos/ASIN/4167656175/premiumcyzo-22/ref=nosim/" target="_blank"><img alt="41P8lTeqLlL._SS500_.jpg" src="http://www.premiumcyzo.com/update_files/41P8lTeqLlL._SS500_.jpg" width="200" height="283" class="mt-image-none" style=""></a><pre class="stxt" style="width: 200px;"><a href="http://www.amazon.co.jp/exec/obidos/ASIN/4167656175/premiumcyzo-22/ref=nosim/" target="_blank"><u>『オウムと私』</u></a>（文藝春秋）</pre></pre>
					
					<p>　ＪＦＫが暗殺された63年は、『Ｘメン』の出版が始まった年であり、黒人の公民権運動のピークだった。南北戦争後も南部では100年近くにわたって人種隔離政策が続いていた。これに対してキング牧師は非暴力デモで戦ったが、アラバマ州政府は暴力でデモを潰そうとした。警官たちが無抵抗の黒人を殴る模様はテレビで全世界に放送され、ＪＦＫは南部の人種隔離をやめさせる公民権法を準備していたが11月に暗殺されてしまった。今回の映画では、ＪＦＫはミュータント差別反対の政策も進めていたので暗殺されたと暗示される。マグニートは弾丸を曲げて狙撃を阻止しようとしたのだ。</p>
					<p>　ＪＦＫが暗殺された63年は、『Ｘメン』の出版が始まった年であり、黒人の公民権運動のピークだった。南北戦争後も南部では100年近くにわたって人種隔離政策が続いていた。これに対してキング牧師は非暴力デモで戦ったが、アラバマ州政府は暴力でデモを潰そうとした。警官たちが無抵抗の黒人を殴る模様はテレビで全世界に放送され、ＪＦＫは南部の人種隔離をやめさせる公民権法を準備していたが11月に暗殺されてしまった。今回の映画では、ＪＦＫはミュータント差別反対の政策も進めていたので暗殺されたと暗示される。マグニートは弾丸を曲げて狙撃を阻止しようとしたのだ。</p>
					
					<pre class="left"><img alt="1407_ikeido_09.jpg" src="http://www.premiumcyzo.com/update_files/1407_ikeido_09.jpg" width="280" height="356" class="mt-image-none" style=""><pre class="stxt" style="width: 280px;">ドラマ『半沢直樹』は面白かったという鈴木氏。池井戸作品は特に読んだことがないそうだが、編集部が持参した『下町ロケット』を持ってパチリ。百田尚樹氏の作品はお好きとのことだ。</pre></pre>
					
					<p>　こうしたマイノリティ問題を反映する『Ｘメン』だが、バイセクシャルを公表しているブライアン・シンガー監督によって、映画ではゲイの苦悩も加わった。ミュータントの青年が家族に打ち明けられなかったり、息子がミュータントだと知った母親が「普通の人になれないの？」と尋ねる描写などはゲイの人々の体験そのものだ。原作コミックでも、ゲイの街として知られるサンフランシスコがミュータントの解放区になるエピソードもあった。</p>
					<p>　こうしたマイノリティ問題を反映する『Ｘメン』だが、バイセクシャルを公表しているブライアン・シンガー監督によって、映画ではゲイの苦悩も加わった。ミュータントの青年が家族に打ち明けられなかったり、息子がミュータントだと知った母親が「普通の人になれないの？」と尋ねる描写などはゲイの人々の体験そのものだ。原作コミックでも、ゲイの街として知られるサンフランシスコがミュータントの解放区になるエピソードもあった。</p>
					<p>――今年4月、「小悪魔ageha」の版元であるインフォレスト社が倒産した。独特すぎる誌面と名物編集長の存在で00年代後半に話題を呼びまくった「ageha」がどうなってしまうのか、と思っていた矢先に今度は「egg」が休刊を発表。いよいよギャル誌は終わりつつあるようだ。あるいは、きゃりーぱみゅぱみゅを生んだ“原宿系”青文字雑誌の隆盛も、ひところに比べれば落ち着き、一方で「CLASSY.」が実売90%を達成するなど再び赤文字的なコンサバへの揺り戻しも見られる。刻々と変化し続ける女性ファッション誌の潮流の中で、現在の女性の消費とライフスタイルに、雑誌はどうコミットできるのか？ 雑誌不況の現在だからこそ考えてみたい。</p>
					
					<p><strong>［座談会出席メンバー］</strong><br>
					<strong>A</strong>：30代女性ライター。90年代末からギャル誌に関わり、E誌ほか、執筆した雑誌は多数。<br>
					<strong>B</strong>：30代女性ライター。A誌などのギャル誌やギャル男誌R、ファッションムックなどで執筆。<br>
					<strong>C</strong>：20代女性編集。人気ギャル雑誌Jに2年在籍。自身もギャル。</p>
					
					<pre class="komidashi"><h4>H4タグ　モデル事務所とブログの功罪……炎上にビビるモデルたち</h4></pre>
					
					<pre class="box-contents"><pre class="box-contents-in">
						<h4 class="box-title">『X-MEN　フューチャー&amp;パスト』</h4>
						<span class="mt-enclosure mt-enclosure-image" style="display: inline;"><pre class="right"><img alt="1407_gal_02.jpg" src="http://www.premiumcyzo.com/update_files/1407_gal_02.jpg" width="200" height="260" class="mt-image-none" style=""></pre></span>
						<big><big><strong>『egg』</strong></big></big><br>発行／大洋図書<br>左上から、2001年10月号、1997年9月号、2014年7月号（休刊号）。上の2冊の頃は「スーパー高校生」ブームの時期であり、巻頭企画は皆制服。ちなみに、97年9月号表紙の左端の女の子は、「CanCam」「姉Can」（小学館）でおなじみ・押切もえさんです。<pre class="clr"><hr></pre>
						<p><span class="mt-enclosure mt-enclosure-image" style="display: inline;"></span></p><pre class="right"><img alt="1407_gal_05.jpg" src="http://www.premiumcyzo.com/update_files/1407_gal_05.jpg" width="200" height="260" class="mt-image-none" style=""></pre><p></p>
						<p><big><big><strong>『Happie nuts』『小悪魔ageha』</strong></big></big><br>
						発行／共にインフォレスト<br>
						左上の「nuts」は、図らずも最終号となった14年5月号。表紙モデルの矢野安奈は、以前は「egg」「Popteen」で活動していた。「ageha」は、「病み」という斬新すぎるテーマとド派手なアートワークが話題になった08年6月号から。</p>
						<pre class="clr"><hr></pre>
					</pre></pre>
				</div><!--entryBody-->
				
				<ul id="sns">
					<li class="hatebu"><a href="http://b.hatena.ne.jp/entry/http://www.premiumcyzo.com/modules/member/2014/07/post_5241/" class="hatena-bookmark-button" data-hatena-bookmark-title="松本人志に次いで、品川ヒロシも大コケ決定!?　吉本興業“映画事業”で危惧される元松竹大物プロデューサーの不甲斐なさ" data-hatena-bookmark-layout="standard" title="このエントリーをはてなブックマークに追加"><img src="http://b.st-hatena.com/images/entry-button/button-only.gif" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="http://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script></li>
					<li class="twitter"><a href="http://twitter.com/share" class="twitter-share-button" data-url="http://lite-ra.com/2014/07/post-216.html" data-text="うさぎが木っ端みじんに…きゃりー効果でバカ売れ中の本がグロい!?" data-count="horizontal" data-lang="ja" data-via="litera_web">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></li>
					<li class="facebook"><div class="fb-like" data-href="http://www.premiumcyzo.com/modules/member/2014/07/post_5241/" data-send="false" data-width="105" data-layout="button_count" data-show-faces="false"></div></li>
					<li class="googleplus"><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script><g:plusone></g:plusone></li>
					<li class="mixicheck"><a href="http://mixi.jp/share.pl" class="mixi-check-button" data-key="2da15db5857c0aa277b1e5a8031e4c337af9bfcc" data-url="http://www.premiumcyzo.com/modules/member/2014/07/post_5241/" data-button="button-1">Check</a><script type="text/javascript" src="http://static.mixi.jp/js/share.js"></script></li>
					</ul>
				
				<div id="pageNav">
					<div>pages</div>
					<ol>
						<li><a href="#">1</a></li>
						<li><span>2</span></li>
						<li><a href="#">3</a></li>
					</ol>
					<span><a href="#">→</a></span>
				</div>
				
				<div id="loginArea">
					<p id="memberLongin"><a href="#">プレミア会員の方はコチラからログイン</a></p>
					<p id="notMember"><a href="#">会員登録して<br>続きを読む</a></p>
				</div>
				
			</div><!--entryMain-->
			
			<div id="relateEntry">
				<div class="boxTitle">関連記事</div>
				<ul>
					<li><h2><a href="#">H2タグ　松本人志に次いで、品川ヒロシも大コケ決定!?　吉本興業“映画事業”で危惧される元松竹大物プロデューサーの不甲斐なさ</a></h2></li>
					<li><h2><a href="#">H2タグ　麻薬のようなアニメ&amp;マンガ実写化を乱発 不振映画業界の2014年ダメ映画予報</a></h2></li>
					<li><h2><a href="#">H2タグ　ヤクザ排除、テレビ局のてなづけ......大﨑洋社長の"社内一斉大清掃"吉本興業の真のタブー</a></h2></li>
					<li><h2><a href="#">H2タグ　お笑いも映画もとことんツマラン!! 松本人志は「もう死んでいる！」</a></h2></li>
					<li><h2><a href="#">H2タグ　吉本興業 大﨑洋社長に聞いた「ITは本当に儲かりまっか？」</a></h2></li>
				</ul>
			</div>
			
			<div id="entryRanking">
				<div class="boxTitle">人気記事ランキング</div>
				<ol>
					<li><span>1</span><a href="#">松本人志に次いで、品川ヒロシも大コケ決定!?　吉本興業“映画事業”で危惧される元松竹大物プロデューサーの不甲斐なさ</a></li>
					<li><span>2</span><a href="#">麻薬のようなアニメ&amp;マンガ実写化を乱発 不振映画業界の2014年ダメ映画予報</a></li>
					<li><span>3</span><a href="#">ヤクザ排除、テレビ局のてなづけ......大﨑洋社長の"社内一斉大清掃"吉本興業の真のタブー</a></li>
					<li><span>4</span><a href="#">お笑いも映画もとことんツマラン!! 松本人志は「もう死んでいる！」</a></li>
					<li><span>5</span><a href="#">吉本興業 大﨑洋社長に聞いた「ITは本当に儲かりまっか？」</a></li>
					<li><span>6</span><a href="#">松本人志に次いで、品川ヒロシも大コケ決定!?　吉本興業“映画事業”で危惧される元松竹大物プロデューサーの不甲斐なさ</a></li>
					<li><span>7</span><a href="#">麻薬のようなアニメ&amp;マンガ実写化を乱発 不振映画業界の2014年ダメ映画予報</a></li>
					<li><span>8</span><a href="#">ヤクザ排除、テレビ局のてなづけ......大﨑洋社長の"社内一斉大清掃"吉本興業の真のタブー</a></li>
					<li><span>9</span><a href="#">お笑いも映画もとことんツマラン!! 松本人志は「もう死んでいる！」</a></li>
					<li><span>10</span><a href="#">吉本興業 大﨑洋社長に聞いた「ITは本当に儲かりまっか？」</a></li>
				</ol>
			</div>
			
			<div id="freeEntry">
				<div class="boxTitle">無料記事</div>
				<div class="bookEtry">
					<div class="text"><a href="#">松本人志に次いで、品川ヒロシも大コケ決定!?　吉本興業“映画事業”で危惧される元松竹大物プロデューサーの不甲斐なさ</a></div>
					<div class="photo"><a href="#"><img alt="#" src="http://dummyimage.com/95x120/0065b8/fff.png&text=photo5" height="120"></a></div>
				</div>
				<div class="bookEtry">
					<div class="text"><a href="#">麻薬のようなアニメ&amp;マンガ実写化を乱発 不振映画業界の2014年ダメ映画予報</a></div>
					<div class="photo"><a href="#"><img alt="#" src="http://dummyimage.com/95x120/0065b8/fff.png&text=photo5" height="120"></a></div>
				</div>
				<div class="bookEtry">
					<div class="text"><a href="#">ヤクザ排除、テレビ局のてなづけ......大﨑洋社長の"社内一斉大清掃"吉本興業の真のタブー</a></div>
					<div class="photo"><a href="#"><img alt="#" src="http://dummyimage.com/95x120/0065b8/fff.png&text=photo5" height="120"></a></div>
				</div>
				<div class="bookEtry">
					<div class="text"><a href="#">お笑いも映画もとことんツマラン!! 松本人志は「もう死んでいる！」</a></div>
					<div class="photo"><a href="#"><img alt="#" src="http://dummyimage.com/95x120/0065b8/fff.png&text=photo5" height="120"></a></div>
				</div>
				<div class="bookEtry">
					<div class="text"><a href="#">吉本興業 大﨑洋社長に聞いた「ITは本当に儲かりまっか？」</a></div>
					<div class="photo"><a href="#"><img alt="#" src="http://dummyimage.com/95x120/0065b8/fff.png&text=photo5" height="120"></a></div>
				</div>
			</div>
			
		</div><!--archive-->
		
		<div id="aside">
			<?php include_once($webRoot."/dev/include/aside_top.html"); ?>
		</div><!--aside-->
	</div><!--contents-->

	<div id="footer">
		<?php include_once($webRoot."/dev/include/footer.html"); ?>
	</div><!--footer-->
</div><!--wrapper-->
<?php include_once($webRoot."/dev/include/gNav.html"); ?>
<?php include_once($webRoot."/dev/include/foot_script.html"); ?>
<script type="text/javascript" src="/_pcRenewal/js/top.min.js" ></script>
</body>
</html>