<?php
$webRoot = $_SERVER["DOCUMENT_ROOT"]."/_pcRenewal";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>グラビアギャラリー｜サイゾーpremium</title>
<?php include_once($webRoot."/dev/include/head_share.html"); ?>
<meta name="description" content="視点をリニューアルする情報サイト「サイゾーpremium」" />
<meta name="keyword" content="サイゾー,芸能,タブー" />
<meta property="fb:admins" content="" />
<meta property="fb:app_id" content="" />
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="website" />
<meta property="og:title" content="サイゾーpremium" />
<meta property="og:description" content="視点をリニューアルする情報サイト「サイゾーpremium」" />
<meta property="og:url" content="http://www.premiumcyzo.com/" />
<meta property="og:site_name" content="" />
<meta property="og:image" content="/img/icon-sns.png" />
<meta property="image_src" content="/img/icon-sns.png" />
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@">
<meta name="twitter:url" content="">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:image" content="/img/icon-sns.png">
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://www.premiumcyzo.com/i/">
</head>
<body id="premiumcyzoCom">
<div id="wrapper" class="gallery">
	<div id="header">
		<h1>h1のテキストを決めてください。</h1>
		<?php include_once($webRoot."/dev/include/header.html"); ?>
	</div><!--header-->

	<div id="contents">
		<div id="archive">
			
			<div id="bread">
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://www.premiumcyzo.com/_pcRenewal/" itemprop="url"><span itemprop="title">サイゾーPremium</span></a></span>
				<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">&nbsp;&gt;&nbsp;<span itemprop="title">グラビアギャラリー</span></span>
			</div>
			
			<div id="galleryMain">
				<div class="gaSubTitle">今月のグラビア</div>
				<div class="galleryMainBox">
					<div id="galleryPhoto"><div id="galleryPhotoInner">
						<a href="#"><img src="http://dummyimage.com/1000x550/0000ff/fff.png&text=Photo_1000_550_1"></a>
						<a href="#"><img src="http://dummyimage.com/1000x550/ff0000/fff.png&text=Photo_1000_550_2"></a>
						<a href="#"><img src="http://dummyimage.com/1000x550/00ff00/fff.png&text=Photo_1000_550_3"></a>
					</div></div>
					<div id="galleryText"><div id="galleryTextInner">
						<a href="#"><h2>H3タグ　タレント1　名前名前名前</h2></a>
						<a href="#"><h2>H3タグ　タレント2　名前名前名前</h2></a>
						<a href="#"><h2>H3タグ　タレント3　名前名前名前</h2></a>
					</div></div>
				</div>
				<div id="moveLeft" class="mover split"></div>
				<div id="moveRight" class="mover split"></div>
			</div>
			
			<div id="galleryPickup">
				<div class="gaSubTitle">グラビアランキング</div>
				<div id="galleryPickupInner">
					<div class="galleryPickupBox"><a href="#">
						<div class="photo"><img src="http://dummyimage.com/316x316/0065b8/fff.png&text=Photo_316"></div>
						<div class="text" id="no1"><h3>H3タグ　タレント　名前名前名前</h3></div>
					</a></div>
					<div class="galleryPickupBox"><a href="#">
						<div class="photo"><img src="http://dummyimage.com/316x316/0065b8/fff.png&text=Photo_316"></div>
						<div class="text" id="no2"><h3>H3タグ　女優　名前名前名前</h3></div>
					</a></div>
					<div class="galleryPickupBox"><a href="#">
						<div class="photo"><img src="http://dummyimage.com/316x316/0065b8/fff.png&text=Photo_316"></div>
						<div class="text" id="no3"><h3>H3タグ　歌手　名前名前名前</h3></div>
					</a></div>
				</div>
			</div>
			
			<div id="galleryArchive"></div>
		</div><!--archive-->
	</div><!--contents-->

	<div id="footer">
		<?php include_once($webRoot."/dev/include/footer.html"); ?>
	</div><!--footer-->
</div><!--wrapper-->
<?php include_once($webRoot."/dev/include/gNav.html"); ?>
<?php include_once($webRoot."/dev/include/foot_script.html"); ?>
<script type="text/javascript" src="/_pcRenewal/js/gallery.min.js" ></script>
</body>
</html>