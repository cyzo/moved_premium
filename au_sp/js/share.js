// UA判定 Android振り分け
function is_mobile () {
  var useragents = [
    'Android',        // 1.5+ Android
    'dream',          // Pre 1.5 Android
    'CUPCAKE'        // 1.5+ Android
    ];
  var pattern = new RegExp(useragents.join('|'), 'i');
  return pattern.test(navigator.userAgent);
}
function is_iOS () {
  var useragents = [
    'iPhone',
    'iPad',
    'iPod'
    ];
  var pattern = new RegExp(useragents.join('|'), 'i');
  return pattern.test(navigator.userAgent);
}
if (is_mobile()) {
  var link = document.createElement('link');
  link.rel = 'stylesheet';
  link.type = 'text/css';
  link.href = '/i/css/android.css';
  document.getElementsByTagName('head')[0].appendChild(link);
}

//bookpassバナー
if (is_mobile()){
	$("#bookpassAndroid").show();
}
if (is_iOS()){
	$("#bookpassIphone").show();
}

// アドレスバーを消す
function doScroll() { if (window.pageYOffset === 0) { window.scrollTo(0,1); } }
window.addEventListener('load', function () { setTimeout(doScroll, 100); }, false);
window.onorientationchange = function() { setTimeout(doScroll, 100); };





$(function () {


$('a[href^=#], area[href^=#]').not('a[href=#], area[href=#]').each(function () {
	// jquery.easing
	jQuery.easing.quart = function (x, t, b, c, d) {
		return -c * ((t = t / d - 1) * t * t * t - 1) + b;
	};
	if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname && this.hash.replace(/#/, '')) {
		var $targetId = $(this.hash),
			$targetAnchor = $('[name=' + this.hash.slice(1) + ']');
		var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
		if ($target) {
			var targetOffset = $target.offset().top;
			$(this).click(function () {
				$('html, body').animate({
					scrollTop: targetOffset
				}, 500, 'quart');
				return false;
			});
		}
	}
});
if (location.hash) {
	var hash = location.hash;
	window.scroll(0, 0)
	$('a[href=' + hash + ']').click();
}



//折りたたみ
$(".toggleBtn").click(function(){
	$(".toggleBtn span").toggleClass('open');
	$(".toggleArea").slideToggle('fast');
})

$(".searchtoggleBtn").click(function(){
	$(".searchArea").slideToggle('fast');
	$('body,html').animate({
					scrollTop: 0
	}, 400);
})


var thisid = $('body').attr("id");
if (thisid == 'page-rensai' || thisid == 'page-interview') {
	$('#categoryList').css('display', 'block');
	$(".toggleBtn span").toggleClass('open');
}





// サイドバー用の折りたたみ
var openBtn =".toggleList span.openBtn";
var hideContents =".toggleList dd ul li ul";

$(hideContents).hide();
$(openBtn).click(function(){
	var index = $(openBtn).index(this);
	$(this).toggleClass("open");
	$(hideContents).eq(index).slideToggle('fast');
}).css("cursor","pointer");




// クリックしたら読み込む
$.autopager({
	autoLoad: false,
	link: 'a.link_next',
	content: '.newArticle',
	load:function(current,next){
        if( current.page >= autoPageMaxNum ){ //最後のページ
            $('.more.loadMore').hide();
        }
    }
});
$('.loadMore a.link_next').click(function() {
	$.autopager('load');
	return false;
});

$.pautopager({
	autoLoad: false,
	link: 'a.limit_next',
	content: '.limitArticle'
});
$('.loadMore a.limit_next').click(function() {
	$.pautopager('load');
	return false;
});


	
//alert( thisid );

});


//ga 
$(function(){
	var saizoUrl; var nowDir; var ob; var sob;
	
	//topか下層か
	saizoUrl = location.pathname;
	if (saizoUrl == "" || saizoUrl == "index.html") {
		nowDir = "top";
	}
	else if (saizoUrl.match(/\/i\/[0-9]{4}\/[0-9]{2}\/((index|[0-9]{1,})\.html)?$/) || saizoUrl.match(/\/i\/backnumber\/(.*\.html)?/)) {
		nowDir = "mon";
	}
	else if (saizoUrl.match(/\/i\/[0-9]{4}\/[0-9]{2}\/.*\.html/)) {
		nowDir = "ent";
	}
	else if (saizoUrl.match(/\/i\/[ -~]*?\/[ -~]*?\/(.*\.html)?/) || saizoUrl.match(/\/i\/[ -~]*?\/(.*\.html)?/)) {
		nowDir = "cat";
	}
	else {
		nowDir = "other";
	}

	function gp(cat, act, lab) {
		var gpCat = cat + "：" + nowDir;
		var gpAct = act != "" ? cat + "：" + nowDir + " / " + act : "out of order";
		var gpLab = lab != "" ? cat + "：" + nowDir + " / " + lab : "";
		_gaq.push(['_trackEvent', gpCat, gpAct, gpLab]);
		console.log("gpCat-> "+gpCat);console.log("gpAct-> "+gpAct);console.log("gpLab-> "+gpLab);console.log("------------------------------");
	}



//nav
	////menu
	$("#menu a").click(function(){
		gp("menu", "", "");
	});
	////categories
	$("#side dl span, #side dd > ul > li > a").click(function(){
		gp("menu", "category", "category / "+$(this).text());
	});
	$("#side dl li li a").click(function(){
		gp("menu", "category", "category / "+$(this).parents("li").find("span").text() + " / " + $(this).text());
	});
	////close
	$("#side .close a").click(function(){
		gp("menu", "close", "");
	});

//logo
	$("#globalHeader h1 a").click(function(){
		gp("logo", "", "");
	});



/*
//おすすめ
	$("section article a").click(function(){
		if ( $(this).parents("section").find("h2").text() == "オススメ記事" ) gp("osusume", "", "");
	});

//ランキング
	$(".rankingSection article a").click(function(){
		gp("ranking", $(".rankingSection article a").index(this)+1, "");
	});

//公式アカウント
	$("section .social a").click(function(){
		gp("snsAcount", $(this).text(), "");
	});

//運営メディア
	$("section .media a").click(function(){
		gp("medias", $(this).text(), "");
	});
*/



//entry
	////関連記事 + リンク
	$("section > .articleList a").click(function(e){
		if ($(this).parents("section").find("h2").text()=="関連記事") {
			gp("relation", "", "");e.stopImmediatePropagation();
		}
		if ($(this).parents("section").find("h2").text()=="関連リンク") {
			gp("outRelation", "", "");e.stopImmediatePropagation();
		}
	});
	////下部カテゴリー
	$("section .categoryList span, section .categoryList dd > ul > li > a").click(function(){
		gp("category", $(this).text(), "");
	});
	$("section .categoryList li li a").click(function(){
		gp("category", $(this).parents("li").find("span").text() + " / " + $(this).text(), "");
	});
	////カテゴリ + タグ
	$("footer .meta a").click(function(){
		if ($(this).parent("dd").prev().text()=="カテゴリ")　{
			sendText="catLink";
		}
		else if ($(this).parent("dd").prev().text() == "タグ")　{
			sendText="tagLink";
		}
		gp(sendText, "", "");
	});

/*
//独自
	$("#globalHeader p.btn a").click(function(e){
		if($(this).attr("href").match(/itunes/)) hoge = "iphone";
		else hoge = "android";
		gp("headApp", hoge, "");e.stopImmediatePropagation();
	});
	$(".gravureList a").click(function(e){
		gp("photoGravure", "photo", "");e.stopImmediatePropagation();
	});
	$("section h2 a[href='/i/cat15/gallery/']").click(function(e){
		gp("photoGravure", "more", "");e.stopImmediatePropagation();
	});
*/


});
