<?php
include ($_SERVER['DOCUMENT_ROOT'].'/au_sp/session_check.php');
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LINEで使えるスタンプ無料プレゼント!!</title>
    <link href="assets/css/common.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>
    // サイト名★
    const siteName = "サイゾーpremium";
    
    // リクエスト先メールアドレス★
    const registMailAddress = "asmp_cp2_03@ist.sf.cybird.ne.jp";
    const cancelMailAddress = "asmp_cp2_unreg_03@ist.sf.cybird.ne.jp";
    
    // 登録リクエスト件名,本文
    const registSubject = "【"+siteName+"】スタンプリクエスト送信";
    const registBody = "このまま送信して下さい。%0d%0a%0d%0aダウンロードページが記載された%0d%0a自動返信メールが、すぐに届きます。";
    
    // 解除リクエスト件名,本文
    const cancelSubject = "【"+siteName+"】キャンペーンメール解約";
    const cancelBody = "今後、キャンペーンメールのご案内が不要な場合は、このまま送信して下さい。";
    
    // loadリサイズ,エフェクト
    $(function(){
        // 解像度
        var deviceWidth = window.innerWidth;
        if (deviceWidth > window.innerHeight) {
            deviceWidth = window.innerHeight;
        }
        const materialWidth = 640;
        const scale = deviceWidth / materialWidth;
        
        $("body").css({zoom:scale});
    });
    $(window).on("load noload", function(){
        setTimeout(scrollBy, 100, 0, 1);
        $("body").css({visibility:"visible"}).fadeTo("normal", 1);
    });
    
    function sendRegistRequest() {
        if ($("#aggrement input[type='checkbox']")[0].checked) {
            // リクエスト送信
            location.href = "mailto:"+registMailAddress+"?subject="+registSubject+"&body="+registBody;
            ga("send", "event", "marsh1506 ", "click", "request-mail", 1); // イベントトラッキング
        } else {
            alert("注意事項に同意いただいた方のみ、リクエストできます");
        }
    }
        
    function sendCancelRequest() {
        // キャンセル送信
        location.href = "mailto:"+cancelMailAddress+"?subject="+cancelSubject+"&body="+cancelBody;
        ga("send", "event", "marsh1506 ", "click", "cancel-mail", 1); // イベントトラッキング
    }
    </script>
    
</head>
<body>
    <div class="center">
        <header>
            サイゾーpremium
        </header>
        <img src="assets/img/page/common/gossip_header.png">
        
        <div style=" text-align: left;">

<table class="menu">
<tr><td align="center" width="20%">
<img src="assets/img/page/common/cyzo_message.jpg">
</td><td>
<a href="http://www.premiumcyzo.com/au_sp/modules/member/2015/06/post_2326/">
<span style="color:#ffff66;" class="sp">★</span></span><span style="color:#696969;"><span style="color:#ff1a66;" class="sp">愛之助､紀香と真剣交際</span><br>
捨てられた熊切あさ美に見る“女性タレント”の宿命と運命とは!?</span></a></td></tr></table>

<table class="menu">
<tr><td align="center" width="20%">
<img src="assets/img/page/common/cyzo_premium_icon2_144B.png">
</td><td>
<a href="
http://www.premiumcyzo.com/au_sp/modules/member/2015/09/post_2413/">
<span style="color:#ffff66;" class="sp">★</span><span style="color:#696969;"><span style="color:#ff1a66;" class="sp">キムタクが共演NG!?</span><br>
同級生だった人気タレント●●●と、共演の可能性は!?</span></span>
</a></td></tr></table>

<table class="menu">
<tr><td align="center" width="20%">
<img src="assets/img/page/common/icon_hosoki.png">
</td><td>
<a href="http://www.6sei.net/smartpass/famous_lp?utm_source=cyzo1&utm_medium=cyzo&utm_campaign=marsh">
<span style="color:#ffff66;" class="sp">★</span></span><span style="color:#696969;"><span style="color:#ff1a66;" class="sp">ズバリ！岡田准一</span><br>
宮崎あおいとの熱愛報道!!修羅場をくぐり抜けてきた二人の相性とは?!</span>
</a></td></tr></table>

<table class="menu">
<tr><td align="center" width="20%">
<img src="assets/img/page/common/icon_hosoki.png">
</td><td>
<a href="http://www.6sei.net/smartpass/famous_lp?utm_source=cyzo2&utm_medium=cyzo&utm_campaign=marsh">
<span style="color:#ffff66;" class="sp">★</span><span style="color:#696969;"><span style="color:#ff1a66;">ズバリ！又吉直樹</span><br>
｢火花｣が芥川賞受賞!!芸人初の快挙も次回作執筆は来年以降にすべき?!</span>
</a></td></tr></table>


<br>

</div>

        <img src="assets/img/page/common/header.png">
<!--
        <div class="margin-1 color-red size-2">
            スマパス会員限定！<br>
            今だけぜ～んぶ無料プレゼント！
        </div>
-->
        
<!--
        <ul id="stamp-list">
               <li><img src="assets/img/obj/hyo/1-1.png"></li>
               <li><img src="assets/img/obj/hyo/1-2.png"></li>
               <li><img src="assets/img/obj/hyo/1-3.png"></li>
               <li><img src="assets/img/obj/hyo/1-4.png"></li>
        </ul>
        
        
        <div class="size-2 margin-1">
            ▲長押しして画像保存してね！【期間限定：7/31まで】
        </div>
        
        <div class="margin-1">
            <img src="assets/img/page/hyo/btn_mail.png" onclick="sendRegistRequest()">
        </div>

-->
    
        <div class="margin-1">
		<!-- 提供画像の横幅が1080pxのため、画面よりはみ出るので、imgタグにスタイルで横幅640pxを指定 -->
            <img src="assets/img/page/hyo/stamp_sample_hyo.png" style="width:640px;">
        </div>
        <div class="size-2 margin-1">
            スタンプの一部を紹介♪<br>
            リクエストを送信して、<br>
            全部もらっちゃおう！<br>

        </div>
        
        <div class="margin-1">
            <img src="assets/img/page/hyo/button_h_2.png" onclick="sendRegistRequest()">
        </div>
        



        
        <div id="aggrement">
            <input type="checkbox" checked><a href="#notice">注意事項</a>を確認の上、タップして送信
            <img style="visibility:hidden; width:0; height:0" src="assets/img/page/common/chk_off.png">
        </div>
        
        <div class="margin-3">
            <img src="assets/img/page/common/title_s0.png">
        </div>
        
        <!-- 細木数子六星占術 -->
        <div class="margin-1">
            <img src="assets/img/page/kuma/campaign.png">
            <a class="btn-download" href="http://www.6sei.net/smartpass/lp/?utm_source=cyzo&utm_medium=cyzo&utm_campaign=marsh">細木数子六星占術で無料ダウンロード</a>
            <div class="left size-1" style="margin-top:-40px">
                「細木数子六星占術」スマートパス認証ページへ遷移します。<br>
                　スマートパス会員の方は無料でご利用頂けます。
            </div>
        </div>
        
        <!-- 鏡リュウジ占星術 -->
        <div class="margin-1">
            <img src="assets/img/page/pony/campaign.png">
            <a class="btn-download" href="http://sp.ryuji.tv/smartpass/numatop/?utm_source=cyzo&utm_medium=cyzo&utm_campaign=marsh">鏡リュウジ占星術で無料ダウンロード</a>
            <div class="left size-1" style="margin-top:-40px">
                「鏡リュウジ占星術」スマートパス認証ページへ遷移します。<br>
                　スマートパス会員の方は無料でご利用頂けます。
            </div>
        </div>        
        <!-- 注意事項 -->
        <a name="notice"></a>
        <div class="center margin-2">
            <img src="assets/img/page/common/title_s1.png">
        </div>
        <div class="left size-1 margin-1">
            
            ① LINEでご利用頂く際はスタンプではなく端末内部の写真を呼び出す操作でご利用頂けます。<br>
            <br>
            ② リクエストメールを送信頂いた方には、メールでスタンプ配信ページをお送りさせて頂きます。<br>
            迷惑メール対策をされている場合は、必ず<span style="background-color:#ffc">@ist.sf.cybird.ne.jp</span>からのメールを受信できるように設定してください。<br>
            メールが届かない場合は、迷惑メール対策されている可能性があります。<br>
            <br>
            ③ リクエストメールを送信頂いた方には、下記のご案内をさせて頂きます。その他の用途には一切使用致しません。<br>
            ・限定スタンプ配信ページのご案内<br>
            ・スタンプ更新のご案内<br>
            ・次回キャンペーンのご案内<br>
            ・CYBIRDのauスマートパスサービスに関するご案内<br>
            ご案内メールの受け取りを希望されない場合は<a href="javascript:sendCancelRequest()">コチラ</a>から解約手続きをお願い致します。<br>
            <br>
            ④ 本キャンペーンはサービス運営者である株式会社<br>
            サイバードが独自に実施しているキャンペーンで、LINE株式会社様とは関係ありません。<br>
            また本キャンペーンは、予告なく終了、または変更する場合がございます。<br>
        </div>
        
        <footer>
            &copy;2015 CYBIRD
        </footer>
    </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-33359286-6', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>